FROM nginx:1.15-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY ./public-html/ /usr/share/nginx/html/
EXPOSE 80 8080